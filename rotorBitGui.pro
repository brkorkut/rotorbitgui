#-------------------------------------------------
#
# Project created by QtCreator 2014-12-07T16:29:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = rotorBitGui
TEMPLATE = app


SOURCES += src/helisim.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    lib/qcustomplot.cpp \
    src/drawplots.cpp \
    src/commandbox.cpp

HEADERS  += src/helisim.h \
    src/mainwindow.h \
    lib/qcustomplot.h \
    src/drawplots.h \
    src/commandbox.h

FORMS    += src/mainwindow.ui

OTHER_FILES += \
    input.txt \
    Bell206.jpg

#macx: LIBS += -L$$PWD/lib/ -lctest

