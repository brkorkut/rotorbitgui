#include "commandbox.h"
#include <QLayout>

commandBox::commandBox(QWidget *parent, int index) :
    QWidget(parent)
{
    QString *groupName = new QString("Pilot Command (");
    groupName->append(QString::number(index+1)).append(")");
    groupBox = new QGroupBox(*groupName,0);
    QFont font("Lucida Grande", 16);
    groupBox->setFont(font);

    cbCommandType = new QComboBox(this);
    cbInputType = new QComboBox(this);
    label1= new QLabel("Command Type");
    label2 = new QLabel("Input Type");
    label3 = new QLabel("Time Offset");
    label4 = new QLabel("Amplitude");
    plotButton = new QPushButton("Plot");
    removeButton = new QPushButton("Remove");
    spinbox1 = new QDoubleSpinBox(this);
    spinbox2 = new QDoubleSpinBox(this);

    QFont font2("Lucida", 12);
    spinbox1->setFont(font2);
    spinbox2->setFont(font2);
    cbCommandType->setFont(font2);
    cbInputType->setFont(font2);
    QStringList cbCmdText;
    QStringList cbInpText;

    //Command list
    cbCmdText <<tr("Lateral")<< tr("Longitudinal")<<tr("Collective")<<tr("Pedal");
    cbInpText <<tr("Step")<< tr("Impulse")<<tr("Doublet")<<tr("Custom");
    cbCommandType->addItems(cbCmdText);
    cbInputType->addItems(cbInpText);

    //connect(button2,SIGNAL(clicked()),this,SLOT(ui->verticalLayout_22->removeWidget(commandGroups[numOfCommands])));

    QHBoxLayout *hbox1 = new QHBoxLayout;
    QHBoxLayout *hbox2 = new QHBoxLayout;
    QHBoxLayout *hbox3 = new QHBoxLayout;
    QHBoxLayout *hbox4 = new QHBoxLayout;
    QVBoxLayout *vbox1 = new QVBoxLayout;
    QVBoxLayout *vbox2 = new QVBoxLayout;
    QVBoxLayout *vbox3 = new QVBoxLayout;
    QGridLayout *grid = new QGridLayout;

    hbox1->addWidget(label1);
    hbox1->addWidget(label2);

    hbox2->addWidget(cbCommandType);
    hbox2->addWidget(cbInputType);

    vbox1->addWidget(label3);
    vbox1->addWidget(spinbox1);

    vbox2->addWidget(label4);
    vbox2->addWidget(spinbox2);

    vbox3->addLayout(hbox1);
    vbox3->addLayout(hbox2);


    hbox3->addLayout(vbox1);
    hbox3->addLayout(vbox2);

    hbox4->addWidget(plotButton);
    hbox4->addWidget(removeButton);

    grid->addLayout(vbox3,1,1,0);
    grid->addLayout(hbox2,2,1,0);
    grid->addLayout(hbox3,3,1,0);
    grid->addLayout(hbox4,4,1,0);

    groupBox->setLayout(grid);
}
