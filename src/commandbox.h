#ifndef COMMANDBOX_H
#define COMMANDBOX_H

#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QGroupBox>

class commandBox : public QWidget
{
    Q_OBJECT
public:
    explicit commandBox(QWidget *parent = 0, int index = 0);
    QGroupBox *groupBox;
    QComboBox *cbCommandType;
    QComboBox *cbInputType;
    QLabel *label1;
    QLabel *label2;
    QLabel *label3;
    QLabel *label4;
    QPushButton *plotButton;
    QPushButton *removeButton;
    QDoubleSpinBox *spinbox1;
    QDoubleSpinBox *spinbox2;
signals:

public slots:

private:
    //QString *groupName;


};

#endif // COMMANDBOX_H
