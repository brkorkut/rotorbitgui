#include "helisim.h"

HeliSim::HeliSim()
{
    rho=0.002378;
    gravity=32.17;
}


Aircraft::Aircraft(){

}

Aircraft::~Aircraft(){

}

void Aircraft::initAircraftModel(int modelNo, Aircraft *model){
    if(modelNo == 1){
        float L2scale	= (10.2/9.5)*(10.2/9.5);

        //Aircraft
        model->modelName = "Bell 407";
        model->Weight	 = 5000; //lb
        model->IX	= 1028;
        model->IY	= 2130*(model->Weight/2400)*L2scale;
        model->IZ	= 1700*(model->Weight/2400)*L2scale;
        model->IXZ	= 300;
        model->FSCG	= 124.6;
        model->WLCG	= 60.4;
        model->BLCG	= 0.0;

        //Rotor Properties
        model->rotorFSMR	= 122.2;
        model->rotorWLMR	= 116.8;
        model->rotorBLMR	= 0.0;
        model->rotorRadius	= 17.5; 			// Rotor radius (ft)
        model->rotorOmega	= 41.43; 			// Rotor speed (rad/sec) 725 ft/sec tip speed in Ham = 41.43 rad/sec
        model->rotorHOffset	= 0.51; 			// Hinge offset (ft)  Should be 2.9% according to Ham = 0.51 ft
        model->rotorSpar	= 1.75; 		// Spar length (distance from hinge to start of rotor blade) (ft) (about 10%)
        model->rotorLiftslope	= 5.73 ; 	//Lift slope of airfoil 1/rad (not used, using NACA 0012)
        model->rotorAlpha0	= 0.0; //Zero Lift Angle of Attack (not used, using NACA 0012)
        model->rotorCd0 	= 0.008; 			//Profile drag coefficient (not used, using NACA 0012)
        model->rotorTwistBR	= 0.0; //Twist distribution Tables
        model->rotorTwistTHETA 	= 0.0;				// twist
        model->rotorChord 	= 13.0/120. ; 			//Blade chord (ft) - Ham
        model->rotorBTL		= 0.97; 				//Tip loss factor (not currently used)
        model->rotorMBETA	= 12.3; 				//First mass moment of blade sl-ft
        model->rotorIBETA	= 143.; 				//Moment of intertia of blade about flap hinge sl-ft^2
        model->rotorWBLADE	= 45.0; 				//Weight of blade (lbs)
        model->rotorALD		= 0.227;  				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorBLD		= 3.242;	 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorCLD		= 12.04; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorDLD02	= 10.0102; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorRLD		= 6.898; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorTHETALDGEO	= 0.0; 			//Parameters describe lag damper geometry (deg) - Same as UH-60A NOT USED

        //Tail rotor properties
        model->trotorFSTR	= 379.1;
        model->trotorBLTR	= 7.05;
        model->trotorWLTR	= 67.5;
        model->trotorCantTR	= 0.0;
        model->trotorOmegaTR	= 2550*2*3.14/60;
        model->trotorRadius	= 65.0/12.0/2.0;
        model->trotorDel3TR	= 45.0;
        model->trotorBTLTR	= 0.97;
        model->trotorA0TR	= 5.73;
        model->trotorLambeta2TR	= 1.0;
        model->trotorSolTR	= (4*5.3/12.0)/(3.14*model->trotorRadius);
        model->trotorGamTR	= 0.3;
        model->trotorTwistTR	= 4.0/3.14/180.0;
        model->trotorFBTR	= 0.700;
        model->trotorDelta0TR	= 0.01;
        model->trotorDelta2TR	= 0.0;

        //Vertical tail
        model->vtailArea	= 9.2;
        model->vtailPhi		= -90;
        model->vtailIndicene	= 5.5;
        model->vtailFS		= 384.0;
        model->vtailBL		= 70.1;
        model->vtailWL		= 0.0;
        model->vtailAL		= 0.0;
        model->vtailCL		= 0.0;
        model->vtailCD		= 0.0;
        //Horiz->ontal tail
        model->htailArea	= 13.4;
        model->htailPhi		= 0.0;
        model->htailIndicene= 0.0;
        model->htailFS		= 302.7;
        model->htailBL		= 0.0;
        model->htailWL		= 65.4;
        model->htailAL		= 0.0;
        model->htailCL		= 0.0;
        model->htailCD		= 0.0;






    }else if(modelNo==2){
        //Aircraft
        model->modelName = "UH 60";
        model->Weight	 = 16400; //lb
        model->IX	= 5629.0;
        model->IY	= 40000.0;
        model->IZ	= 37200.0;
        model->IXZ	= 1670.0;
        model->FSCG	= 360.4;
        model->WLCG	= 247.2;
        model->BLCG	= 0.0;

        //Rotor Properties

        model->rotorFSMR	= 341.2;
        model->rotorWLMR	= 315.0;
        model->rotorBLMR	= 0.0;
        model->rotorRadius	= 26.3; 			// Rotor radius (ft)
        model->rotorOmega	= 27.0; 			// Rotor speed (rad/sec) 725 ft/sec tip speed in Ham = 41.43 rad/sec
        model->rotorHOffset	= 1.25; 			// Hinge offset (ft)  Should be 2.9% according to Ham = 0.51 ft
        model->rotorSpar	= 2.25; 		// Spar length (distance from hinge to start of rotor blade) (ft) (about 10%)
        model->rotorLiftslope	= 5.73 ; 	//Lift slope of airfoil 1/rad (not used, using NACA 0012)
        model->rotorAlpha0	= 0.0; //Zero Lift Angle of Attack (not used, using NACA 0012)
        model->rotorCd0 	= 0.006; 			//Profile drag coefficient (not used, using NACA 0012)
        model->rotorTwistBR	= 0.0; //Twist distribution Tables
        model->rotorTwistTHETA 	= 0.0;				// twist
        model->rotorChord 	= 1.73 ; 			//Blade chord (ft) - Ham
        model->rotorBTL		= 0.97; 				//Tip loss factor (not currently used)
        model->rotorMBETA	= 86.7; 				//First mass moment of blade sl-ft
        model->rotorIBETA	= 1512.6; 				//Moment of intertia of blade about flap hinge sl-ft^2
        model->rotorWBLADE	= 256.9; 				//Weight of blade (lbs)
        model->rotorALD		= 0.227;  				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorBLD		= 3.242;	 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorCLD		= 12.04; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorDLD02	= 10.0102; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorRLD		= 6.898; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
        model->rotorTHETALDGEO	= 0.0; 			//Parameters describe lag damper geometry (deg) - Same as UH-60A NOT USED

        //Tail rotor properties
        model->trotorFSTR	= 732.0;
        model->trotorBLTR	= -14.0;
        model->trotorWLTR	= 324.7;
        model->trotorCantTR	= 0.0;
        model->trotorOmegaTR	= 124.62*2*3.14/60;
        model->trotorRadius	= 5.5;
        model->trotorDel3TR	= 0.7002;
        model->trotorBTLTR	= 0.92;
        model->trotorA0TR	= 0.75;
        model->trotorLambeta2TR	= 1.0;
        model->trotorSolTR	= 0.1875;
        model->trotorGamTR	= 70.0;
        model->trotorTwistTR	= 17.2*3.14/180;
        model->trotorFBTR	= 0.700;
        model->trotorDelta0TR	= 0.0087;
        model->trotorDelta2TR	= 0.4;

        //Vertical tail
        model->vtailArea	= 32.3;
        model->vtailPhi		= -90;
        model->vtailIndicene	= 0.0;
        model->vtailFS		= 695.0;
        model->vtailBL		= 0.0;
        model->vtailWL		= 273.0;
        model->vtailAL		= 0.0;
        model->vtailCL		= 0.0;
        model->vtailCD		= 0.0;
        //Horiz->ontal tail
        model->htailArea	= 45.0;
        model->htailPhi		= 0.0;
        model->htailIndicene= 0.0;
        model->htailFS		= 700.4;
        model->htailBL		= 0.0;
        model->htailWL		= 244.0;
        model->htailAL		= 0.0;
        model->htailCL		= 0.0;
        model->htailCD		= 0.0;

    }
}
