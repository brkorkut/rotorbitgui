#ifndef HELISIM_H
#define HELISIM_H
#include <QString>
class HeliSim
{
public:
    HeliSim();//Air density
    float rho;
    float gravity;
//    void sum(int* a, int* b, int* c);

};

class Aircraft
{
public:
    Aircraft();
    ~Aircraft();
    void initAircraftModel(int modelNo, Aircraft *model);

    QString modelName;
    //Aircraft properties
    float Weight;
    float Mass;
    float IX;
    float IY;
    float IZ;
    float IXZ;
    float FSCG;
    float WLCG;
    float BLCG;


    //Rotor Properties
    float rotorFSMR;
    float rotorWLMR;
    float rotorBLMR;
    float rotorRadius; 			// Rotor radius (ft)
    float rotorOmega; 		// Rotor speed (rad/sec) 725 ft/sec tip speed in Ham = 41.43 rad/sec
    float rotorHOffset; 	// Hinge offset (ft)  Should be 2.9% according to Ham = 0.51 ft
    float rotorSpar; 		// Spar length (distance from hinge to start of rotor blade) (ft) (about 10%)
    float rotorLiftslope; 	//Lift slope of airfoil 1/rad (not used, using NACA 0012)
    float rotorAlpha0; //Zero Lift Angle of Attack (not used, using NACA 0012)
    float rotorCd0; 			//Profile drag coefficient (not used, using NACA 0012)
    float rotorTwistBR; //Twist distribution Tables
    float rotorTwistTHETA;				// twist
    float rotorChord; 			//Blade chord (ft) - Ham
    float rotorBTL; 				//Tip loss factor (not currently used)
    float rotorMBETA; 				//First mass moment of blade sl-ft
    float rotorIBETA; 				//Moment of intertia of blade about flap hinge sl-ft^2
    float rotorWBLADE; 				//Weight of blade (lbs)
    float rotorALD;  				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
    float rotorBLD; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
    float rotorCLD; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
    float rotorDLD02; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
    float rotorRLD; 				//Parameters describe lag damper geometry (in) - Same as UH-60A NOT USED
    float rotorTHETALDGEO; 			//Parameters describe lag damper geometry (deg) - Same as UH-60A NOT USED

    //Tail rotor properties
    float trotorRadius;
    float trotorOmegaTR;
    float trotorSolTR;
    float trotorGamTR;
    float trotorTwistTR;
    float trotorFSTR;
    float trotorBLTR;
    float trotorWLTR;
    float trotorA0TR;
    float trotorCantTR;
    float trotorDel3TR;
    float trotorBTLTR;
    float trotorLambeta2TR;
    float trotorFBTR;
    float trotorDelta0TR;
    float trotorDelta2TR;

    //Vertical tail
    float vtailArea;
    float vtailPhi;
    float vtailIndicene;
    float vtailFS;
    float vtailBL;
    float vtailWL;
    float vtailAL;
    float vtailCL;
    float vtailCD;
    //Horizontal tail
    float htailArea;
    float htailPhi;
    float htailIndicene;
    float htailFS;
    float htailBL;
    float htailWL;
    float htailAL;
    float htailCL;
    float htailCD;

};

#endif // HELISIM_H
