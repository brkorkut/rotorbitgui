#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "helisim.h"
#include "drawplots.h"

#include <QFile>
#include <iostream>
#include <QMessageBox>
#include <QTextStream>
#include <QFileDialog>
#include <QComboBox>
#include <QSpinBox>
#include <QWidget>
#include <QFile>
#include <QXmlStreamWriter>


MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
    initTabs();
    updateGraphicsView();
    initAircraftTab();
    initCommandInputsTab();
    initInflowTab();
    connect(modelListCB,SIGNAL(currentIndexChanged(int)),this,SLOT(updateAircraftTreeWidget(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initCommandInputsTab(){
    initPlots();
    for (int i = 0; i < 4 ; i++)
    {
        pilotInput[i] = new commandBox(this, i);
        connect(pilotInput[i]->plotButton,SIGNAL(clicked()),this,SLOT(redrawPlot()));
        ui->verticalLayout_22->insertWidget(i,pilotInput[i]->groupBox,0,0);
    }
    //connect(pilotInput[0]->plotButton,SIGNAL(clicked()),this,SLOT(redrawPlot()));


}


void MainWindow::redrawPlot(){
    QCustomPlot *plot = new QCustomPlot;
//    switch (plotID) {
//    case 0:
        plot = ui->commandPlot0;
//        break;
//    case 1:
//        plot = ui->commandPlot1;
//        break;
//    case 2:
//        plot = ui->commandPlot2;
//        break;
//    case 3:
//        plot = ui->commandPlot3;
//        break;
//    default:
//        break;
//    }

    double timeOffset = pilotInput[0]->spinbox1->value();
    double amplitute = pilotInput[0]->spinbox2->value();
    QVector<double> x(500), y(500);

    for(int i = 0 ; i<500 ; i++){
        if(i <= int(timeOffset)*10){
            y[i] = 0.0;
        }else{
            y[i] = amplitute;
        }
        x[i]=0.1*i;
    }
    setupSinglePlot(plot,x,y);
    plot->replot();

}

void MainWindow::initInflowTab(){
}

void MainWindow::initAircraftTab(){
    modelListCB = new QComboBox(this);
    QStringList cbText;

    //Aircraft list - this should be automated
    cbText <<tr("Select Model")<< tr("Bell 206")<<tr("UH-60")<<tr("User Specified");

    modelListCB->addItems(cbText);
    QTreeWidgetItem *toplevel = new QTreeWidgetItem(ui->treeWidget);
    ui->treeWidget->addTopLevelItem(toplevel);
    ui->treeWidget->setItemWidget(toplevel,1,modelListCB);

    unitsListCB = new QComboBox(this);
    QStringList cbText2;
    cbText2 << tr("Imperial")<<tr("SI");
    unitsListCB->setMinimumWidth(10);
    unitsListCB->addItems(cbText2);
    unitsListCB->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
    ui->treeWidget->setItemWidget(toplevel,2,unitsListCB);

    //Resize treeWidget header sections
    ui->treeWidget->header()->resizeSection(1,125);
    ui->treeWidget->header()->resizeSection(2,50);

    initAircraftTreeWidget();
}

void MainWindow::initPlots(){
    QVector<double> x(500),power(500),thrust(500), lat(500), lon(500), col(500), ped(500);

    for (int i=0 ; i<500 ; i++){
        x[i]=i*0.1;
        if(i<=10)
            lat[i] = 0.0;
        else{
            lat[i] = 1.0;
        }

        if(i>=100 && i<=120){
            ped[i]=0.6;
        }else if(i > 120 && i < 140){
            ped[i]=0.4;
        } else {
            ped[i]=0.5;
        }

        col[i]=0.5;
        lon[i]=0.0;

        power[i]=qExp(-i/150.0);
        thrust[i]=5*i+100;
    }

    setupSinglePlot(ui->trimPlot, x, power);
    setupSinglePlot(ui->trimPlot1,x, thrust);
    ui->trimPlot->xAxis->setLabel("Time(sec)");
    ui->trimPlot->yAxis->setLabel("Power (hp)");
    ui->trimPlot1->xAxis->setLabel("Time(sec)");
    ui->trimPlot1->yAxis->setLabel("Thrust (lbf)");

    initCommandPlot0();
    initCommandPlot1();
    initCommandPlot2();
    initCommandPlot3();
}

void MainWindow::initCommandPlot0(){
    //set plot data
    QVector<double> x(500), y(500);
    for(int i=0 ; i<500 ; i++){
        x[i] = 0;
        y[i] = 0;
    }
    setupSinglePlot(ui->commandPlot0, x, y);
    // set axis labels
    ui->commandPlot0->xAxis->setLabel("Time (sec)");
    ui->commandPlot0->yAxis->setLabel("Lateral Input (%)");
    // set title of plot:
    ui->commandPlot0->plotLayout()->insertRow(0);
    ui->commandPlot0->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->commandPlot0, "Pilot Command (1)"));
}


void MainWindow::initCommandPlot1(){
    //set plot data
    QVector<double> x(500), y(500);
    for(int i=0 ; i<500 ; i++){
        x[i] = 0;
        y[i] = 0;
    }
    setupSinglePlot(ui->commandPlot1, x, y);
    // set axis labels
    ui->commandPlot1->xAxis->setLabel("Time (sec)");
    ui->commandPlot1->yAxis->setLabel("Lateral Input (%)");
    // set title of plot:
    ui->commandPlot1->plotLayout()->insertRow(0);
    ui->commandPlot1->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->commandPlot1, "Pilot Command (2)"));
}

void MainWindow::initCommandPlot2(){
    //set plot data
    QVector<double> x(500), y(500);
    for(int i=0 ; i<500 ; i++){
        x[i] = 0;
        y[i] = 0;
    }
    setupSinglePlot(ui->commandPlot3, x, y);
    // set axis labels
    ui->commandPlot2->xAxis->setLabel("Time (sec)");
    ui->commandPlot2->yAxis->setLabel("Lateral Input (%)");
    // set title of plot:
    ui->commandPlot2->plotLayout()->insertRow(0);
    ui->commandPlot2->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->commandPlot2, "Pilot Command (3)"));
}


void MainWindow::initCommandPlot3(){
    //set plot data
    QVector<double> x(500), y(500);
    for(int i=0 ; i<500 ; i++)
    {
        x[i] = 0;
        y[i] = 0;
    }
    setupSinglePlot(ui->commandPlot3, x, y);
    // set axis labels
    ui->commandPlot3->xAxis->setLabel("Time (sec)");
    ui->commandPlot3->yAxis->setLabel("Lateral Input (%)");
    // set title of plot:
    ui->commandPlot3->plotLayout()->insertRow(0);
    ui->commandPlot3->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->commandPlot3, "Pilot Command (4)"));
}




void MainWindow::setupSinglePlot(QCustomPlot *customPlot, QVector<double> x, QVector<double> y0){
    demoName = "Simple Demo";
    // add two new graphs and set their look:
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    customPlot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    customPlot->graph(0)->setData(x, y0);
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    customPlot->graph(0)->rescaleAxes(true,false);
    // same thing for graph 1, but only enlarge ranges (in case graph 1 is smaller than graph 0):
    // Note: we could have also just called customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}

void MainWindow::readFile(QString fileName){
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        QMessageBox::information(0,"Error",file.errorString());

    QString line;
    QTextStream textStream(&file);
    QTreeWidgetItem *currentItem;

    while (!textStream.atEnd()){
        line=textStream.readLine();
        QStringList parts = line.split("\t",QString::KeepEmptyParts);
        if (parts.length() == 2)
        {
            //AddChild(currentItem,parts[0],parts[1]);
        }else if(parts.length() == 1){
            int treeID=parts[0].toInt();
            std::cout<<treeID<<std::endl;
            switch (treeID){
            //Maind Rotor
            case 1:
                currentItem=mainRotor;
                break;
                //Tail Rotor
            case 2:
                currentItem=tailRotor;
                break;
                //Fuselage
            case 3 :
                currentItem=fuselage;
                break;
                //Hor. Tail
            case 4:
                currentItem=horTail;
                break;
                //Ver. Tail
            case 5:
                currentItem=verTail;
                break;
            default:
                break;
            }
        }
    }
    ui->treeWidget->expandAll();
    file.close();
}

void MainWindow::AddRoot(QString Name, QString Value){
    QTreeWidgetItem *item = new QTreeWidgetItem(ui->treeWidget);
    item->setText(0,Name);
    item->setText(1,Value);
}

void MainWindow::AddChild(QTreeWidgetItem *currentItem, QString Name, QString value, QString unit){
    QTreeWidgetItem *item = new QTreeWidgetItem();
    item->setText(0,Name);
    item->setText(1,value);
    item->setText(2,unit);
    currentItem->addChild(item);
}

void MainWindow::on_actionNew_File_triggered(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File:"),"../../.",
                                                    tr("*.txt files *.txt"));
    readFile(fileName);
}

void MainWindow::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column){
    Qt::ItemFlags tmp= item->flags();
    //Only second column is editable
    if(column == 1){
        item->setFlags(tmp | Qt::ItemIsEditable);
        ui->treeWidget->editItem(item,column);
    }
}


void MainWindow::initAircraftTreeWidget(){
    //Arrange treewidget
    //ui->treeWidget->setColumnCount(3);
    //ui->treeWidget->setHeaderLabels(QStringList() << "Variable" << "Value"<<"Units");
    //Initialize treewidget items
    aircraft = new QTreeWidgetItem(ui->treeWidget);
    mainRotor = new QTreeWidgetItem(ui->treeWidget);
    tailRotor = new QTreeWidgetItem(ui->treeWidget);
    fuselage = new QTreeWidgetItem(ui->treeWidget);
    horTail = new QTreeWidgetItem(ui->treeWidget);
    verTail = new QTreeWidgetItem(ui->treeWidget);


    //Set Root item Labels
    aircraft->setText(0,"AIRCRAFT");
    mainRotor->setText(0,"MAIN ROTOR");
    tailRotor->setText(0,"TAIL ROTOR");
    fuselage->setText(0,"FUSELAGE");
    horTail->setText(0,"HORIZONTAL TAIL");
    verTail->setText(0,"VERTICAL TAIL");

    QFont font("Lucida",13, QFont::Bold, true);
    font.setItalic(false);
    aircraft->setFont(0,font);
    mainRotor->setFont(0,font);
    tailRotor->setFont(0,font);
    fuselage->setFont(0,font);
    horTail->setFont(0,font);
    verTail->setFont(0,font);


    //Aircraft section
    AddChild(aircraft,"Name","","");			//0
    AddChild(aircraft,"Weight","0.00","lbs");	//1
    AddChild(aircraft,"Body Intertias","","");	//2
    AddChild(aircraft->child(2),"IX","0.00","slug-ft2");
    AddChild(aircraft->child(2),"IY","0.00","slug-ft2");
    AddChild(aircraft->child(2),"IZ","0.00","slug-ft2");
    AddChild(aircraft->child(2),"IXZ","0.00","slug-ft2");
    AddChild(aircraft,"Location of CG.","","");	//3
    AddChild(aircraft->child(3),"Fuselage Station","0.00","in");
    AddChild(aircraft->child(3),"Water Line","0.00","in");
    AddChild(aircraft->child(3),"Buttock Line","0.00","in");



    //Rotor section
    //const QChar MathSymbolDelta(0x0394);
    AddChild(mainRotor,"Main Rotor Location ","","");	//0
    AddChild(mainRotor->child(0),"Fuselage Station","0.00","in");
    AddChild(mainRotor->child(0),"Water Line","0.00","in");
    AddChild(mainRotor->child(0),"Buttock Line","0.00","in");
    AddChild(mainRotor,"Rotor Radius","0.00","ft");		//1
    AddChild(mainRotor,"RPM","0.00","");				//2
    AddChild(mainRotor,"Hinge offset","0.00","ft");	 	//3

    AddChild(mainRotor,"Blade Properties","","");		//4
    AddChild(mainRotor->child(4),"Spar Length","0.00","ft");
    AddChild(mainRotor->child(4),"Chord Length","0.00","ft");
    AddChild(mainRotor->child(4),"Lift Curve Slope","0.00","");
    AddChild(mainRotor->child(4),"Zero Lift AoA Coefficient","0.00","");
    AddChild(mainRotor->child(4),"Profile Drag Coefficient","0.00","");
    AddChild(mainRotor->child(4),"Twist - Root","0.00","deg");
    AddChild(mainRotor->child(4),"Twist - Theta ","0.00","deg");
    AddChild(mainRotor->child(4),"Weight","0.00","lbs");
    AddChild(mainRotor->child(4),"Tip Loss Factor","0.00","");
    AddChild(mainRotor->child(4),"First Mass Moment","0.00","slug-ft");
    AddChild(mainRotor->child(4),"Moment of Inertia about Flap Hinge","0.00","slug-ft2");

    AddChild(mainRotor,"Lag Damper Properties","","");	//5
    AddChild(mainRotor->child(5),"ALD","0.00","in");
    AddChild(mainRotor->child(5),"BLD","0.00","in");
    AddChild(mainRotor->child(5),"CLD0","0.00","in");
    AddChild(mainRotor->child(5),"DLD02","0.00","in");
    AddChild(mainRotor->child(5),"RLD","0.00","in");
    AddChild(mainRotor->child(5),"ThetaLDGeo","0.00","deg");

    //Tail rotor properties
    AddChild(tailRotor,"Tail Rotor Location:","","");			//0
    AddChild(tailRotor->child(0),"Fuselage Station:","0.00","in");
    AddChild(tailRotor->child(0),"Water Line:","0.00","in");
    AddChild(tailRotor->child(0),"Buttock Line:","0.00","in");
    AddChild(tailRotor,"Rotor Radius","0.00","ft");				//1
    AddChild(tailRotor,"Rotor Speed","0.00","");				//2
    AddChild(tailRotor,"Solidity","0.00","");					//3
    AddChild(tailRotor,"Gama","0.00","deg");						//4
    AddChild(tailRotor,"Twist","0.00","deg");						//5
    AddChild(tailRotor,"Blade Lift Curve Slope","0.00","");	//6
    AddChild(tailRotor,"Cant Angle","0.00","deg");				//7
    AddChild(tailRotor,"Delta-3 Angle","0.00","deg");				//8
    AddChild(tailRotor,"Blade Tip Loss Factor","0.00","");						//9
    AddChild(tailRotor,"Lambeta","0.00","");					//10
    AddChild(tailRotor,"FB","0.00","");						//11
    AddChild(tailRotor,"Delta0","0.00","");					//12
    AddChild(tailRotor,"Delta2","0.00","");					//13

    AddChild(fuselage,"Location of Fuselage","","");
    AddChild(fuselage->child(0),"Fuselage Station","0.00","in");
    AddChild(fuselage->child(0),"Water Line","0.00","in");
    AddChild(fuselage->child(0),"Buttock Line","0.00","in");
    AddChild(fuselage,"Equivalent Flat Plane Areas","","");
    AddChild(fuselage->child(1),"X-Axis","0.00","ft2");
    AddChild(fuselage->child(1),"Y-Axis","0.00","ft2");
    AddChild(fuselage->child(1),"Z-Axis","0.00","ft2");


    //Vertical tail
    AddChild(verTail,"Location of Vert. Tail","","");
    AddChild(verTail->child(0),"Fuselage Station","0.00","in");
    AddChild(verTail->child(0),"Water Line","0.00","in");
    AddChild(verTail->child(0),"Buttock Line","0.00","in");
    AddChild(verTail,"Area","0.00","ft2");
    AddChild(verTail,"Phi","0.00","deg");
    AddChild(verTail,"Indicence","0.00","deg");
    AddChild(verTail,"Lift Curve Slope","0.00","");
    AddChild(verTail,"Lift Coefficient","0.00,","");
    AddChild(verTail,"Drag Coefficient","0.00","");
    //Horizontal tail
    AddChild(horTail,"Location of Hor. Tail","","");
    AddChild(horTail->child(0),"Fuselage Station","0.00","in");
    AddChild(horTail->child(0),"Water Line","0.00","in");
    AddChild(horTail->child(0),"Buttock Line","0.00","in");
    AddChild(horTail,"Area","0.00","ft2");
    AddChild(horTail,"Phi","0.00","deg");
    AddChild(horTail,"Indicence","0.00","deg");
    AddChild(horTail,"Lift Curve Slope","0.00","");
    AddChild(horTail,"Lift Coefficient","0.00","");
    AddChild(horTail,"Drag Coefficient","0.00","");

    ui->treeWidget->header()->setStretchLastSection(true);
    //ui->treeWidget->expandAll();


}


void MainWindow::updateAircraftTreeWidget(int selectedModel){
    if (selectedModel >> 0){
        Aircraft *model = new Aircraft;
        if (selectedModel == 1){
            initAircraft(selectedModel);
            model = Bell206;
        }
        else if(selectedModel == 2){
            initAircraft(selectedModel);
            model = UH60;
        }


        //Update related widget item childs
        aircraft->child(0)->setText(1,model->modelName);
        aircraft->child(1)->setText(1,QString::number(model->Weight));
        //skip 1
        aircraft->child(2)->child(0)->setText(1,QString::number(model->IX));
        aircraft->child(2)->child(1)->setText(1,QString::number(model->IY));
        aircraft->child(2)->child(2)->setText(1,QString::number(model->IZ));
        aircraft->child(2)->child(3)->setText(1,QString::number(model->IXZ));
        //skip
        aircraft->child(3)->child(0)->setText(1,QString::number(model->FSCG));
        aircraft->child(3)->child(1)->setText(1,QString::number(model->WLCG));
        aircraft->child(3)->child(2)->setText(1,QString::number(model->BLCG));

        //position
        mainRotor->child(0)->child(0)->setText(1,QString::number(model->rotorFSMR));
        mainRotor->child(0)->child(1)->setText(1,QString::number(model->rotorWLMR));
        mainRotor->child(0)->child(2)->setText(1,QString::number(model->rotorBLMR));
        //rotor
        mainRotor->child(1)->setText(1,QString::number(model->rotorRadius));
        mainRotor->child(2)->setText(1,QString::number(model->rotorOmega));
        mainRotor->child(3)->setText(1,QString::number(model->rotorHOffset));
        //blade prop
        mainRotor->child(4)->child(0)->setText(1,QString::number(model->rotorSpar));
        mainRotor->child(4)->child(1)->setText(1,QString::number(model->rotorChord));
        mainRotor->child(4)->child(2)->setText(1,QString::number(model->rotorLiftslope));
        mainRotor->child(4)->child(3)->setText(1,QString::number(model->rotorAlpha0));
        mainRotor->child(4)->child(4)->setText(1,QString::number(model->rotorCd0));
        mainRotor->child(4)->child(5)->setText(1,QString::number(model->rotorTwistBR));
        mainRotor->child(4)->child(6)->setText(1,QString::number(model->rotorTwistTHETA));
        mainRotor->child(4)->child(7)->setText(1,QString::number(model->rotorWBLADE));
        mainRotor->child(4)->child(8)->setText(1,QString::number(model->rotorBTL));
        mainRotor->child(4)->child(9)->setText(1,QString::number(model->rotorMBETA));
        mainRotor->child(4)->child(10)->setText(1,QString::number(model->rotorIBETA));
        //lag prop
        mainRotor->child(5)->child(0)->setText(1,QString::number(model->rotorALD));
        mainRotor->child(5)->child(1)->setText(1,QString::number(model->rotorBLD));
        mainRotor->child(5)->child(2)->setText(1,QString::number(model->rotorCLD));
        mainRotor->child(5)->child(3)->setText(1,QString::number(model->rotorDLD02));
        mainRotor->child(5)->child(4)->setText(1,QString::number(model->rotorRLD));
        mainRotor->child(5)->child(5)->setText(1,QString::number(model->rotorTHETALDGEO));

        tailRotor->child(0)->child(0)->setText(1,QString::number(model->trotorFSTR));
        tailRotor->child(0)->child(1)->setText(1,QString::number(model->trotorWLTR));
        tailRotor->child(0)->child(2)->setText(1,QString::number(model->trotorBLTR));
        tailRotor->child(1)->setText(1,QString::number(model->trotorRadius));
        tailRotor->child(2)->setText(1,QString::number(model->trotorOmegaTR));
        tailRotor->child(3)->setText(1,QString::number(model->trotorSolTR));
        tailRotor->child(4)->setText(1,QString::number(model->trotorGamTR));
        tailRotor->child(5)->setText(1,QString::number(model->trotorTwistTR));
        tailRotor->child(6)->setText(1,QString::number(model->trotorA0TR));
        tailRotor->child(7)->setText(1,QString::number(model->trotorCantTR));
        tailRotor->child(8)->setText(1,QString::number(model->trotorDel3TR));
        tailRotor->child(9)->setText(1,QString::number(model->trotorBTLTR));
        tailRotor->child(10)->setText(1,QString::number(model->trotorLambeta2TR));
        tailRotor->child(11)->setText(1,QString::number(model->trotorFBTR));
        tailRotor->child(12)->setText(1,QString::number(model->trotorDelta0TR));
        tailRotor->child(13)->setText(1,QString::number(model->trotorDelta2TR));


        verTail->child(0)->child(0)->setText(1,QString::number(model->vtailFS));
        verTail->child(0)->child(1)->setText(1,QString::number(model->vtailWL));
        verTail->child(0)->child(2)->setText(1,QString::number(model->vtailBL));
        verTail->child(1)->setText(1,QString::number(model->vtailArea));
        verTail->child(2)->setText(1,QString::number(model->vtailPhi));
        verTail->child(3)->setText(1,QString::number(model->vtailIndicene));
        verTail->child(4)->setText(1,QString::number(model->vtailAL));
        verTail->child(5)->setText(1,QString::number(model->vtailCL));
        verTail->child(6)->setText(1,QString::number(model->vtailCD));


        horTail->child(0)->child(0)->setText(1,QString::number(model->vtailFS));
        horTail->child(0)->child(0)->setText(1,QString::number(model->htailWL));
        horTail->child(0)->child(0)->setText(1,QString::number(model->htailBL));
        horTail->child(1)->setText(1,QString::number(model->htailArea));
        horTail->child(2)->setText(1,QString::number(model->htailPhi));
        horTail->child(3)->setText(1,QString::number(model->htailIndicene));
        horTail->child(4)->setText(1,QString::number(model->htailAL));
        horTail->child(5)->setText(1,QString::number(model->htailCL));
        horTail->child(6)->setText(1,QString::number(model->htailCD));
    }
}

void MainWindow::initAircraft(int modelNo){
    if (modelNo==1){
        Bell206 = new Aircraft;
        Bell206->initAircraftModel(1,Bell206);
    }else if(modelNo==2){
        UH60 = new Aircraft;
        UH60->initAircraftModel(2,UH60);
    }
}


void MainWindow::initTabs(){
    ui->tabWidget->setTabText(0,"Aircraft Model");
    ui->tabWidget->setTabText(1,"Initialization");
    ui->tabWidget->setTabText(2,"Inflow Model");
    ui->tabWidget->setTabText(3,"Command Inputs");
    ui->tabWidget->setTabText(4,"Simulation");
    ui->tabWidget->setStyleSheet(QString("QTabWidget::tab-bar {alignment: left; }"));
}

void MainWindow::updateGraphicsView(){
    scene=new QGraphicsScene;
    pixmap = new QPixmap("../../../../../Bell206.jpg");
    scene->addPixmap(*pixmap);
    ui->graphicsView->setScene(scene);
    scene->setSceneRect(0,0,1000,1000);
    ui->graphicsView->fitInView(scene->itemsBoundingRect(),Qt::KeepAspectRatioByExpanding);
    ui->graphicsView->show();
}

void MainWindow::SaveXMLFile(){
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Aircraft Model"), "../../model.xml",tr("Xml files (*.xml)"));
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("main");
    xmlWriter.writeStartElement("aircraft");
    xmlWriter.writeTextElement("stationLine","");
    xmlWriter.writeTextElement("bottomLine","");
    xmlWriter.writeTextElement("waterLine","");
    xmlWriter.writeTextElement("weight","");
    xmlWriter.writeTextElement("mass","");
    xmlWriter.writeTextElement("Ixx","");
    xmlWriter.writeTextElement("Iyy","");
    xmlWriter.writeTextElement("Izz","");
    xmlWriter.writeTextElement("Ixz","");
    xmlWriter.writeTextElement("Ixy","");
    xmlWriter.writeTextElement("Iyz","");
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("mainRotor");
    xmlWriter.writeTextElement("hubStationLine","");
    xmlWriter.writeTextElement("hubBottomLine","");
    xmlWriter.writeTextElement("hubWaterLine","");
    xmlWriter.writeTextElement("radius",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("chordMid",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("chordRoot",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("chordTip",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("bladeTwist",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("numberOfBlades",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("RPM",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("hubPreconeAngle",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("bladeFlappingIntertia",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("blade1stMassMoment",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("hingeOffset",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("sparLength",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("bladeProfileDragCoeffient",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("bladeLiftCurveSlope",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("shaftTiltAboutYAxis",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("shaftTiltAboutxAxis",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("delta3Angle",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("delta0Angle",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("singleBladeWeight","");
    xmlWriter.writeTextElement("rotationDirection","");
    xmlWriter.writeTextElement("bladeSegmentChord","");
    xmlWriter.writeTextElement("bladeSegmentArea","");
    xmlWriter.writeTextElement("bladeSegmentLiftCoef","");
    xmlWriter.writeTextElement("bladeSegmentDragCoef","");
    xmlWriter.writeTextElement("bladeTipLossFactor","");
    xmlWriter.writeTextElement("flappingSpringStiffness","");
    xmlWriter.writeTextElement("flappingDamperRate","");
    xmlWriter.writeEndElement();

    xmlWriter.writeStartElement("tailRotor");
    xmlWriter.writeTextElement("hubStationLine","");
    xmlWriter.writeTextElement("hubBottomLine","");
    xmlWriter.writeTextElement("hubWaterLine","");
    xmlWriter.writeTextElement("radius",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("chordMid",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("numberOfBlades",mainRotor->child(0)->text(1));
    xmlWriter.writeTextElement("RPM",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("flappingHingeOffset",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("shaftTiltAboutXAxis",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("bladeLiftCurveSlope",mainRotor->child(1)->text(1));
    xmlWriter.writeTextElement("bladeTipLossFactor","");
    xmlWriter.writeTextElement("bladeTwist","");
    xmlWriter.writeTextElement("tailRotorBlockageFromVerticalTail","");
    xmlWriter.writeTextElement("airspeedBreakpointForBloackageFactor","");
    xmlWriter.writeEndElement();
    xmlWriter.writeStartElement("fuselage");
    xmlWriter.writeTextElement("stationLine","");
    xmlWriter.writeTextElement("bottomLine","");
    xmlWriter.writeTextElement("waterLine","");
    xmlWriter.writeTextElement("equivalentFlatPlateAreaXAxis","");
    xmlWriter.writeTextElement("equivalentFlatPlateAreaYAxis","");
    xmlWriter.writeTextElement("equivalentFlatPlateAreaZAxis","");
    xmlWriter.writeEndElement();
    xmlWriter.writeStartElement("horizontalTail");
    xmlWriter.writeTextElement("stationLine","");
    xmlWriter.writeTextElement("bottomLine","");
    xmlWriter.writeTextElement("waterLine","");
    xmlWriter.writeTextElement("chord","");
    xmlWriter.writeTextElement("aspectRatio","");
    xmlWriter.writeTextElement("spanEfficienyFactor","");
    xmlWriter.writeTextElement("referenceSurfaceArea","");
    xmlWriter.writeTextElement("exposedSurfaceArea","");
    xmlWriter.writeTextElement("liftCoeffAtZeroAngleofAttack","");
    xmlWriter.writeTextElement("liftCurveSlope","");
    xmlWriter.writeTextElement("maximumLiftCoeff","");
    xmlWriter.writeEndElement();
    xmlWriter.writeStartElement("verticalTail");
    xmlWriter.writeTextElement("stationLine","");
    xmlWriter.writeTextElement("bottomLine","");
    xmlWriter.writeTextElement("waterLine","");
    xmlWriter.writeTextElement("incidenceAngle","");
    xmlWriter.writeTextElement("liftCoeffAtZeroAngleofAttack","");
    xmlWriter.writeTextElement("liftCurveSlope","");
    xmlWriter.writeTextElement("maximumLiftCoeff","");
    xmlWriter.writeTextElement("referenceSurfaceArea","");
    xmlWriter.writeEndElement();
    xmlWriter.writeStartElement("environment");
    xmlWriter.writeTextElement("gravity","" );
    xmlWriter.writeTextElement("airDensityAtSeaLevel","" );
    xmlWriter.writeTextElement("temperatureAtSeaLevel","" );
    xmlWriter.writeEndElement();
    xmlWriter.writeEndElement();

    xmlWriter.writeEndDocument();

    file.close();
    //  ShowXmlOnScreen();
    statusBar()->showMessage(tr("Xml Saved"));
}



void MainWindow::on_actionSave_Model_triggered(){
    SaveXMLFile();
}

void MainWindow::on_saveModelButton_clicked(){
    SaveXMLFile();
}

void MainWindow::on_addCommandButton_clicked()
{
    //numOfCommands++;
    //ui->verticalLayout_22->insertWidget(numOfCommands,createCommandGroup(numOfCommands),0,0);
    //commandBox *commandObject = new commandBox;
    //ui->verticalLayout_22->insertWidget(1,commandObject,0,0);
    //ui->verticalLayout_15->insertWidget();

}

