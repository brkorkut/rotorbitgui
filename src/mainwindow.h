#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGraphicsView>
#include "helisim.h"
#include <QComboBox>
#include <QGroupBox>
#include "drawplots.h"
#include "commandbox.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Aircraft *Bell206;
    Aircraft *UH60;


private slots:
    void readFile(QString fileName);
    void AddRoot(QString Name, QString Value);
    void AddChild(QTreeWidgetItem *parentItem, QString Name, QString value, QString unit);
    void on_actionNew_File_triggered();
    void initAircraftTreeWidget();
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item,int column);
    void initAircraft(int selectedModel);
    void updateAircraftTreeWidget(int);
    void updateGraphicsView();
    void initTabs();
    void SaveXMLFile();
    void on_actionSave_Model_triggered();
    void on_saveModelButton_clicked();
    void initPlots();
    void setupSinglePlot(QCustomPlot *customPlot, QVector<double> a,QVector<double> b);
    void on_addCommandButton_clicked();
    void initCommandInputsTab();
    void initInflowTab();
    void initAircraftTab();
    void initCommandPlot0();
    void initCommandPlot1();
    void initCommandPlot2();
    void initCommandPlot3();
    void redrawPlot();

private:
    Ui::MainWindow *ui;
    QTreeWidgetItem *mainRotor;
    QTreeWidgetItem *tailRotor;
    QTreeWidgetItem *horTail;
    QTreeWidgetItem *verTail;
    QTreeWidgetItem *fuselage;
    QTreeWidgetItem *aircraft;
    QComboBox *modelListCB;
    QComboBox *unitsListCB;
    QGraphicsScene *scene;
    QPixmap *pixmap;
    QString demoName;
    QGroupBox *commandGroups[4];
    commandBox *pilotInput[4];
    int numOfCommands;
};

#endif // MAINWINDOW_H
